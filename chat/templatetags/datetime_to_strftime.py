from django import template


register = template.Library()


@register.filter
def convert_dtm_to_str(value):
    return value.strftime("%H:%M")
