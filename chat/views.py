from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from chat.forms.login import LoginForm
from chat.forms.register import RegisterForm
from chat.models import Chat, Message


@login_required
def index(request):
    admin_chats = Chat.objects.filter(is_closed=False, is_resolved=False)
    user_chats = request.user.chats.filter(is_closed=False,
                                           is_resolved=False)
    return render(request, "chat/index.html",
                  {"admin_chats": admin_chats, "user_chats": user_chats})


@login_required
def room(request, room_name):
    try:
        chat = Chat.objects.get(name=room_name)
    except ObjectDoesNotExist:
        chat = Chat.objects.create(name=room_name, user=request.user)

    username = request.user.username
    return render(request, "chat/room.html",
                  {"chat": chat, "room_name": room_name,
                   "username": username})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, 'chat/register.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request=request, username=cd['username'],
                                password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('index')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'chat/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('login')


def mark_as_read(request):
    chat_name = request.GET.get('room_name')

    messages = Message.objects.filter(chat__name=chat_name, is_read=False)
    messages.update(is_read=True)

    messages = Message.objects.filter(chat__name=chat_name, is_read=True)
    messages = [
        {'content': message.content,
         'is_read': message.is_read,
         'sender': message.sender.username,
         'created_at': message.created_at}
        for message in messages
    ]
    return JsonResponse({'messages': messages})
