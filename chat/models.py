from django.db import models
from django.contrib.auth import get_user_model


class Chat(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                             related_name='chats', blank=True, null=True)
    operator = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                                 related_name='operator_chats', blank=True,
                                 null=True)
    is_closed = models.BooleanField(default=False)
    is_resolved = models.BooleanField(default=False)

    def __str__(self):
        return f'Chat {self.name}'


class Message(models.Model):
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE,
                             related_name='messages')
    sender = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                               related_name='sent_messages')
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return f'Message {self.content}'
