import json

from asgiref.sync import sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from chat.models import Message, Chat


@sync_to_async
def get_chat(room_name):
    chat = Chat.objects.get(name=room_name)
    return chat


@sync_to_async
def create_message(**kwargs):
    return Message.objects.create(**kwargs)


@sync_to_async
def close_chat(chat_name):
    chat = Chat.objects.get(name=chat_name)
    chat.is_closed = True
    chat.is_resolved = True
    chat.save()


@sync_to_async
def mark_as_read(message_id):
    message = Message.objects.get(message_id)
    message.is_read = True
    message.save()


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = "chat_%s" % self.room_name
        self.chat = await get_chat(self.room_name)
        self.user = self.scope['user']
        await self.channel_layer.group_add(self.room_group_name,
                                           self.channel_name)

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'user_join',
                'user': self.user.username,
            }
        )
        await self.accept()

    async def user_join(self, event):
        username = event["user"]
        message = f'{username} has joined the chat'
        await self.send(text_data=json.dumps({'message': message,
                                              "event_type": event['type']}))

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.room_group_name,
                                               self.channel_name)

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]

        if not self.user.is_authenticated:
            return

        saved_message = await create_message(sender=self.user, chat=self.chat,
                                             content=message)
        chat_name = saved_message.chat.name

        await self.channel_layer.group_send(
            self.room_group_name, {"type": "chat_message",
                                   "message": message,
                                   "created_at": f'{saved_message.created_at.strftime("%H:%M")}',
                                   'username': self.user.username,
                                   "chat_name": chat_name,
                                   "message_id": saved_message.id}
        )

    async def chat_message(self, event):
        if event["message"] == "Завершить чат":
            await self.send(text_data=json.dumps(
                {"message": event["message"]})
            )
            await close_chat(event["chat_name"])
        else:
            message = event["message"]
            message_id = event["message_id"]
            created_at = event["created_at"]
            username = event["username"]
            # Send message to WebSocket
            await self.send(text_data=json.dumps({"message": message,
                                                  "created_at": created_at,
                                                  "username": username,
                                                  "message_id": message_id}))
